package cn.uncode.springcloud.admin.model.eureka;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EurekaInstanceBo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3493678136836204166L;
	
	private long id;
	
	private String name;
	
	private int availabilityZones;
	
	private String canaryFlag;
	
	private String status;
	
	private String instanceId;
	
	private String ipAddr;
	
	private String port;
	
	private String statusPageUrl;
	
	private long lastUpdatedTime;
	
	

}
