package cn.uncode.springcloud.admin.dal.system;

import cn.uncode.springcloud.admin.model.system.dto.CanaryDTO;

import cn.uncode.dal.external.CommonDAL;
 /**
 * service接口类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-06-03
 */
public interface CanaryDAL extends CommonDAL<CanaryDTO> {

}